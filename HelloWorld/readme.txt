ReadMe - Code Samples for Chapter 1, .NET Application Architectures

To build and run the source code from Chapter 1, you need to install the .NET Core Command Line (CLI) Tools.
Please download and install the tools from https://github.com/dotnet/cli. For Windows, you will find an MSI package that you can install on your Windows system: https://dotnetcli.blob.core.windows.net/dotnet/beta/Installers/Latest/dotnet-win-x64.latest.exe

With upcoming updates of Visual Studio, you can run the samples using Visual Studio.
Please check the Wrox code downloads for updates.

For code comments and issues please check https://github.com/ProfessionalCSharp/ProfessionalCSharp6

Thank you!