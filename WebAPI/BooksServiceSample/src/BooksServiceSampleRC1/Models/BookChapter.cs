﻿using System;

namespace BooksServiceSampleRC1.Models
{
    public class BookChapter
    {
        public string Id { get; set; }

        public int Number { get; set; }
        public string Title { get; set; }
        public int Pages { get; set; }
    }
}
