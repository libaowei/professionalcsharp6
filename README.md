# ProfessionalCSharp6
Code samples for the Wrox book Professional C# 6

Use the RC1 branch to open the samples with Visual Studio 2015 and ASP.NET 5 RC 1 tools. The master branch contains newer RC2 samples that has some issues using it from Visual Studio 2015 with the RC 1 tools.

Samples are updated to .NET 5 Core RC. To compile and run the samples you need:
* Visual Studio 2015
* ASP.NET 5 RC1 Tools (https://get.asp.net/ - click the ASP.NET 5 RC, install for Windows)
* Upgrade to the runtime using >dnvm upgrade from the command line 
* dotnet Tools (https://github.com/dotnet/cli, download and installe the latest MSI package)

With some samples, additional NuGet feeds might be needed. Configure these with Visual Studio 2015:
* .NET Core: https://dotnet.myget.org/F/dotnet-core/api/v3/index.json
* ASP.NET Core: https://www.myget.org/F/aspnetmaster/api/v2
* ASP.NET Core 1.0: https://www.myget.org/F/aspnetcidev/api/v3/index.json

Samples that have been updated to netstandardapp1.5, currently you can't use Visual Studio for compilation. Use the dotnet tools (CLI) instead.

Samples available for these chapters (sample folders):

* Chapter 1 - .NET Application Architectures (HelloWorld)
* Chapter 2 - Core C# (CoreCSharp)
* Chapter 3 - Objects and Types (ObjectsAndTypes)
* Chapter 4 - Inheritance (Inheritance)
* Chapter 5 - Managed and Unmanaged Resources (Resources)
* Chapter 6 - Generics (Generics)
* Chapter 7 - Arrays and Tuples (Arrays)
* Chapter 8 - Operators and Casts (OperatorsAndCasts)
* Chapter 9 - Delegates, Lambdas, and Events (Delegates)
* Chapter 10 - Strings and Regular Expressions (StringsAndRegularExpressions)
* Chapter 11 - Collections (Collections)
* Chapter 12 - Special Collections (SpecialCollections)
* Chapter 13 - Language Integrated Query (LINQ)
* Chapter 14 - Errors and Exceptions (ErrorsAndExceptions)
* Chapter 15 - Asynchronous Programming (Async)
* Chapter 16 - Reflection, Metadata, and Dynamic Programming (ReflectionAndDynamic)
* Chapter 17 - Visual Studio 2015 (no code)
* Chapter 18 - .NET Compiler Platform (CompilerPlatform)
* Chapter 19 - Testing (Testing)
* Chapter 20 - Diagnostics and Application Insights (Diagnostics)
* Chapter 21 - Tasks and Parallel Programming (Parallel)
* Chapter 22 - Task Synchronization (Synchronization)
* Chapter 23 - Files and Streams (FilesAndStreams)
* Chapter 24 - Security (Security)
* Chapter 25 - Networking (Networking)
* Chapter 26 - Composition (Composition)
* Chapter 27 - XML and JSON (XMLAndJSON)
* Chapter 28 - Localization (Localization)
* Chapter 29 - Core XAML (XAML)
* Chapter 30 - Styling XAML Apps (StylesAndResources)
* Chapter 31 - Patterns with XAML Apps (Patterns) 
* Chapter 32 - Windows Apps: User Interface (WindowsApps)
* Chapter 33 - Advanced Windows Apps (AdvancedWindowsApps)
* Chapter 34 - Windows Desktop Applications with WPF (WPF)
* Chapter 35 - Creating Documents with WPF (WPFDocuments)
* Chapter 36 - Deploying Windows Apps (DeploymentWindows)
* Chapter 37 - ADO.NET (ADONET)
* Chapter 38 - Entity Framework Core (EntityFramework)
* Chapter 39 - Windows Services (Services)
* Chapter 40 - ASP.NET Core (ASPNET)
* Chapter 41 - ASP.NET MVC (ASPNETMVC)
* Chapter 42 - ASP.NET Web API (WebAPI)
* Chapter 43 - WebHooks and SignalR (SignalRAndWebHooks)
* Chapter 44 - Windows Communication Foundation (WCF)
* Chapter 45 - Deploying Websites and Services (DeploymentWeb)
